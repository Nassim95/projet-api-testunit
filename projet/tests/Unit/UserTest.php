<?php

namespace Tests\Unit;

use App\Models\User;
use Carbon\Carbon;
use PHPUnit\Framework\TestCase;

class UserTest extends TestCase
{
    public function testUserIsValid()
    {
        $this->user = new User([
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::create(1999, 12, 11)->toDateString()
        ]);
        $this->assertTrue($this->user->isValid());
    }

    public function testNoValidEmailNoValide()
    {
        $this->user = new User([
            'name' => 'Bilal',
            'email' => 'bilal',
            'password' => 'passpass',
            'birthday' => Carbon::create(1999, 12, 11)->toDateString()
        ]);
        $this->assertFalse($this->user->isValid());
    }

    public function testNotValidNoName(){
        $this->user = new User([
            'name' => null,
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::create(1999, 12, 11)->toDateString()
        ]);
        $this->assertFalse($this->user->isValid());
    }

    public function testUserIsTooYoung()
    {
        $this->user = new User([
            'name' => 'Bilal',
            'email' => 'bilal',
            'password' => 'passpass',
            'birthday' => Carbon::create(2012, 12, 11)->toDateString()
        ]);
        $this->assertFalse($this->user->isValid());
    }
}
