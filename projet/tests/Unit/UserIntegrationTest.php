<?php

namespace Tests\Unit;

use Carbon\Carbon;
use App\Models\User;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;

class UserIntegrationTest extends TestCase
{
    public function testUserIsValid()
    {
        $user = [
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::create(1999, 12, 11)->toDateString()
        ];

        $client = new Client();
        $response = $client->request("POST", "localhost/login", $user);

        assertEquals(201, $response->getStatusCode()); 
    }

    // Undone from authenticate.php
    public function testNoValidEmailNoValide()
    {
        $user = [
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::create(1999, 12, 11)->toDateString()
        ];

        $client = new Client();
        $response = $client->request("POST", "localhost/login", $user);

        assertEquals(500, $response->getStatusCode());
        assertEquals("L'email de l'utilisateur n'est pas valide", $response->getBody());  
    }

    // DONE
    public function testUserNoExist()
    {
        $user = [
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::create(1999, 12, 11)->toDateString()
        ];

        $client = new Client();
        $response = $client->request("POST", "localhost/login", $user);

        assertEquals(500, $response->getStatusCode());
        assertEquals("L'utilisateur n'existe pas en base", $response->getBody());  
    }

    // DONE
    public function testUserIsLoggedIn()
    {
        $user = [
            'name' => 'nassim',
            'email' => 'nassim@movie.com',
            'password' => 'password',
            'birthday' => Carbon::create(1999, 12, 11)->toDateString()
        ];

        $client = new Client();
        $response = $client->request("POST", "localhost/login", $user);

        assertEquals(201, $response->getStatusCode());
        assertEquals("L'utilisateur est bien connecté", $response->getBody());  
    }

    // Undone from /ui/Authenticate.php
    public function testNotValidNoName()
    {
        $user = [
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::create(1999, 12, 11)->toDateString()
        ];

        $client = new Client();
        $response = $client->request("POST", "localhost/login", $user);

        assertEquals(201, $response->getStatusCode());
        assertEquals("Vous devez renseigné le champ nom", $response->getBody());  
    }

    // Undone from /ui/RegistersUsers.php
    public function testUserIsTooYoung()
    {
        $user = [
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::create(2012, 12, 11)->toDateString()
        ];

        $client = new Client();
        $response = $client->request("POST", "localhost/login", $user);

        assertEquals(500, $response->getStatusCode());
        assertEquals("L'utilisateur est bien connecté", $response->getBody());  
    }
}