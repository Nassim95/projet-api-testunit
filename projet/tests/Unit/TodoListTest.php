<?php

namespace Tests\Unit;

use App\Models\Item;
use App\Models\TodoList;
use Tests\TestCase;
use App\Models\User;
use Carbon\Carbon;

use function PHPUnit\Framework\assertNotFalse;



class TodoListTest extends TestCase
{

    public function testIsValid()
    {
        $this->todoList = new TodoList([
            'name' => 'Swapit Project TodoList',
            'description' => 'To do list du projet swapit'
        ]);

        $this->assertTrue($this->todolist->isValid());
    }

    public function testAddItem()
    {
        $this->item = new Item([
            'name' => 'Tache1',
            'content' => 'Description de la tache1',
            'created_at' => Carbon::now()->subHour(2)
        ]);

        $this->user = new User([
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::now()->subDecades(21)->toDateString()
        ]);

        $this->todoList = $this->getMockBuilder(TodoList::class)
            ->onlyMethods(['getNumberItems', 'getLastItem'])
            ->getMock();
        $this->todolistMocked->expects($this->once())
            ->method('getNumberItems')
            ->willReturn(1);


        $canAddItem = $this->todoList->canAddItem($this->item);
        assertNotFalse($canAddItem);
    }

    public function testNoValidItemFull()
    {
        $this->item = new Item([
            'name' => 'Tache1',
            'content' => 'Description de la tache1',
            'created_at' => Carbon::now()->subHour(2)
        ]);

        $this->user = new User([
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::now()->subDecades(21)->toDateString()
        ]);

        $this->todoList = $this->getMockBuilder(TodoList::class)
            ->onlyMethods(['getNumberItems', 'getLastItem'])
            ->getMock();
        $this->todolistMocked->expects($this->once())
            ->method('getNumberItems')
            ->willReturn(10);

        $canAddItem = $this->todoList->canAddItem($this->item);
        assertNotFalse($canAddItem);
    }

    public function testNoValidItemAddToRecently()
    {
        $this->item = new Item([
            'name' => 'Tache1',
            'content' => 'Description de la tache1',
            'created_at' => Carbon::now()->subHour(2)
        ]);

        $this->user = new User([
            'name' => 'Bilal',
            'email' => 'bilal@swapit.com',
            'password' => 'passpass',
            'birthday' => Carbon::now()->subDecades(21)->toDateString()
        ]);

        $this->todoList = $this->getMockBuilder(TodoList::class)
            ->onlyMethods(['getNumberItems', 'getLastItem'])
            ->getMock();
        $this->todolistMocked->expects($this->once())
            ->method('getLastItem')
            ->willReturn(Carbon::now()->subMinutes(10));

        $canAddItem = $this->todoList->canAddItem($this->item);
        assertNotFalse($canAddItem);
    }
}
