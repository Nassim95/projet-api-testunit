<?php

namespace Tests\Unit;

use App\Movie;
use Carbon\Carbon;
use App\Models\User;
use GuzzleHttp\Client;
use PHPUnit\Framework\TestCase;
use Illuminate\Support\Facades\Http;
use App\Http\Controllers\MovieController;
use App\Http\Services\Tmdb;

class MovieIntegrationTest extends TestCase
{
    private $tmdbMocked;
    private $movieMocked;

    protected function setUp(): void
    {
        parent::setUp();

        $this->tmdbMocked = $this->getMockBuilder(Tmdb::class)
            ->onlyMethods([
                'getPopularMovies', 
                'getTopRated', 
                'getPopularSeries', 
                'getGenreName',
                'getMovieByGenre',
                'setHttp'
            ])
            ->getMock();

            $this->tmdbMocked = $this->getMockBuilder(Tmdb::class)
            ->onlyMethods([
                'getPopularMovies', 
                'getTopRated', 
                'getPopularSeries', 
                'getGenreName',
                'getMovieByGenre',
                'setHttp'
            ])
            ->getMock();
    }

    public function testWrongApiKey(){
        $this->tmdbMocked->setHttp('zzzzz');

        $request = $this->tmdbMocked->getPopularMovies();

        assertEquals(401, $request->getStatusCode());
        assertEquals("Invalid API key: You must be granted a valid key.", $request->getBody());
    }

    public function testApiKeyEmpty(){
        $this->expectException('Exception');
        $this->expectExceptionMessage('clé API vide');

        $this->tmdbMocked->setHttp('');
    }

    public function testGetMovieByGenreResultEmpty($genre){
        $genre = "toto";

        $request = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/movie/top_rated?with_genres='.$genre);

        assertEquals("", $request->getBody(), "aucune donnée n'a été retournée");
    }

    public function testGetMovieByGenreIsNegative(){        
        $genre = -1;
        assertEquals("", $this->movieMocked->getMovieByGenre($genre));

        $this->expectException('Exception');
        $this->expectExceptionMessage('la variable genre doit être positive');
    }

    public function testNotGetPopularMovies(){
        $request = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/movie/popularr');

        assertEquals(404, $request->getStatusCode());
        assertEquals("The resource you requested could not be found.", $request->getBody());
    }

    public function testNotGetPopularSeries(){
        $request = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/movie/tvv');

        assertEquals(404, $request->getStatusCode());
        assertEquals("The resource you requested could not be found.", $request->getBody());
    }

    public function testNotGetTopRated(){
        $request = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/movie/top_ratedd');

        assertEquals(404, $request->getStatusCode());
        assertEquals("The resource you requested could not be found.", $request->getBody());
    }
}