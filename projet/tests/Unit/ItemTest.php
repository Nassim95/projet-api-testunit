<?php

namespace Tests\Unit;

use App\Models\Item;
use PHPUnit\Framework\TestCase;

class ItemTest extends TestCase
{

    public function testIsValid()
    {
        $this->item = new Item([
            'name' => 'Tache 1',
            'content' => 'Decription de la Tache 1'
        ]);
        $this->assertTrue($this->item->isValid());
    }

    public function testNoValidNoName()
    {
        $this->item = new Item([
            'name' => null,
            'content' => 'Decription de la Tache 1'
        ]);
        $this->assertFalse($this->item->isValid());
    }

    public function testNoValidDescriptiontTooLong()
    {
        $this->item = new Item([
            'name' => null,
            'content' => str_repeat("a", 2000)
        ]);
        $this->assertFalse($this->item->isValid());
    }
}
