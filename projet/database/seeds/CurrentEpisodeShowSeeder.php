<?php

use Illuminate\Database\Seeder;

class CurrentEpisodeShowSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\UsersCurrentEpisodeShow::class, 5)->create();
    }
}
