<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Favori;
use Faker\Generator as Faker;

$factory->define(Favori::class, function (Faker $faker) {
    return [
        'id_user' =>$faker->numberBetween(1,135),
        'id_show' =>$faker->randomElement([7052, 87689,2734 ,54155,60735, 1622,1396,2410]),
    ];
});

// 556574,8619,72545,6795,454626,475557

// 70523
// 87689
// 2734
// 54155
// 60735
// 1622
// 1396
// 2410