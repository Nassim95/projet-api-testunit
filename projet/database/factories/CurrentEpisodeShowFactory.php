<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\UsersCurrentEpisodeShow;
use Faker\Generator as Faker;

$factory->define(UsersCurrentEpisodeShow::class, function (Faker $faker) {
    return [
        'id_user' => $faker->numberBetween(1,135),
        'id_show' => $faker->randomElement([7052, 87689,2734 ,54155,60735, 1622,1396,2410]),
        'season' => $faker->numberBetween(1,2),
        'episode' => $faker->numberBetween(1,4),
    ];
});
