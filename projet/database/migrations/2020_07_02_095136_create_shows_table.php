<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShowsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('shows', function (Blueprint $table) {
            $table->integer('id')->unique();
            $table->string('name');
            $table->float('popularity', 3, 3);
            $table->integer('vote_count');
            $table->boolean('video');
            $table->string('poster_path');
            $table->boolean('adult');
            $table->string('backdrop_path');
            $table->string('original_language');
            $table->string('origin_country');
            $table->string('original_name');
            $table->float('vote_average', 2, 1);
            $table->string('overview');
            $table->date('first_air_date');
            $table->integer('season_number');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('shows');
    }
}
