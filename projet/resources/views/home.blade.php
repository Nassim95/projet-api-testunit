@extends('layouts.app')

@section('content')
<div class="container">
    <div class="popular-movies">
    @if(!empty($recommendedMovies))
        <h2 class="ml-4">{{ __('Film recommandés') }}</h2>
                <div class="row text-center" style="display:flex; flex-wrap:nowrap; overflow:auto;">
                    @foreach ($recommendedMovies as $movie)
                        <x-movie-recommended-card :movie="$movie"/>
                    @endforeach
                </div>
            <br/>
    @endif
    @if(!empty($recommendedShows))
        <h2 class="ml-4">{{ __('Séries recommandés') }}</h2>
                <div class="row text-center" style="display:flex; flex-wrap:nowrap; overflow:auto;">
                    @foreach ($recommendedShows as $show)
                        <x-show-recommended-card :show="$show"/>
                    @endforeach
                </div>
            <br/>
    @endif
        <h2 class="ml-4">Films Populaires</h2>
            <div class="row text-center" style="display:flex; flex-wrap:nowrap; overflow:auto;">
                @foreach ($popularMovies as $movie)
                    <x-movie-card :movie="$movie"/>
                @endforeach
            </div>
        <br/>
        <h2 class="ml-4">Séries Populaires</h2>
        <div class="row text-center" style="display:flex; flex-wrap:nowrap; overflow:auto;">
            @foreach ($popularShows as $show)
                <x-show-card :show="$show"/>
            @endforeach
        </div>
    </div>
    <br/>
</div>
@endsection
