@extends('layouts.app')

@section('content')
<div class="container">
    <div class="popular-movies">
        <h2 class="ml-4">Films Favori</h2>
            <div class="row text-center" style="display:flex; flex-wrap:nowrap; overflow:auto;">
                @foreach ($favResult as $fav)
                    <x-movie-fav-card :fav="$fav"/>
                @endforeach
            </div>
    </div>
</div>
@endsection