@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Seances</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    <a href="{{ route('home') }}"> Go to the home.</a>
                    <form action="{{ route('add_seance') }}" method='post'>
                        <form action="{{ route('add_seance') }}" method="post">
                        @csrf
                        Date et heure de la séance : <input type="datetime-local" name=debut_seance><br>
                        Film :
                        <select name=film>
                            @foreach ($films as $film)
                                <option value="{{ $film->id_film }}">{{ $film->titre }}</option>
                            @endforeach
                        </select><br>

                        Salle :
                        <select name=salle>
                            @foreach ($salles as $salle)
                                <option value="{{ $salle->id_salle }}">{{ $salle->nom_salle }}</option>
                            @endforeach
                        </select><br>

                        Ouvreur :
                        <select name=ouvreur>
                            @foreach ($ouvreurs as $ouvreur)
                                <option value="{{ $ouvreur->id_personne }}">{{ $ouvreur->nom }} {{ $ouvreur->prenom }}</option>
                            @endforeach
                        </select><br>

                        Technicien :
                        <select name=technicien>
                            @foreach ($techniciens as $technicien)
                                <option value="{{ $technicien->id_personne }}">{{ $technicien->nom }} {{ $technicien->prenom }}</option>
                            @endforeach
                        </select><br>

                        Nettoyeur :
                        <select name=nettoyeur>
                            @foreach ($nettoyeurs as $nettoyeur)
                                <option value="{{ $nettoyeur->id_personne }}">{{ $nettoyeur->nom }} {{ $nettoyeur->prenom }}</option>
                            @endforeach
                        </select><br>

                        <br><input type="submit" value="Enregistrer la séance !">
                        </form>

                        <br>

                        Liste des séances :

                        <br>

                        <ul>
                        @foreach($seances as $seance)
                        <li><b>{{ $seance->film->titre }}</b> {{ $seance->debut_seance }} <i>{{ $seance->salle->nom_salle }}</i></li>
                        @endforeach
                    
                </div>
            </div>
        </div>
    </div>
</div>
@endsection