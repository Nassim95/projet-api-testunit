@extends('layouts.app')

@section('content')

<div class="movie-info col-12">
    <div class="col-md-6" style="margin:auto;">
        <div class="card flex-md-row mb-4 box-shadow h-md-250 row justify-content-center">
            <div class="card-body d-flex flex-column align-items-start col-6">
                <br/>
                <h4 class="text-bold">Synopsis :</h4>
                <p class="card-text mb-auto">
                    {{ $movie->overview }}
                </p>
                <p class="card-text mb-auto">
                    {{\Carbon\Carbon::parse($movie['release_date'])->format('M d, Y')}}
                </p>
                <p class="card-text">
                    @foreach ($movie->genres as $genre)
                        {{ $genre->name }}
                        @if (!$loop->last) 
                        , 
                        @endif
                    @endforeach
                </p>
                <a href="{{ route('home') }}" type="button" class="btn btn-sm btn-outline-secondary">Retour</a>
            </div>
            <div style="width: 50%;margin: auto;"">
                <img class="bd-placeholder-img card-img-top" src="{{'https://image.tmdb.org/t/p/w500/'.$movie['poster_path']}}"> </img>
            </div>
        </div>
    </div>
    <div style="display: flex; justify-content:center;">
        <h2 class="text-bold text-primary">Trailer :</h2>
    </div>
    <div style="display: flex;
        justify-content: center;
        align-items: center;">
        <iframe style width="820" height="450" src="https://www.youtube.com/embed/{{$movie->videos['results'][1]['key']}}"></iframe>
    </div>
</div>
<div class="d-flex align-item-center">
    @if(!empty($recommendedMovies))
        <div class="row text-center col-6" style="display:flex; margin:auto; flex-wrap:nowrap; overflow:auto;">
            @foreach ($recommendedMovies as $movie)
                <x-movie-recommended-card :movie="$movie"/>
            @endforeach
        </div>
        <br/>
    @endif
</div>
<br/>
<div class="movie-info d-flex justify-content-center"> 
    <div class="col-8 ">
        <h5>Commentaires</h5>
        @forelse($movie->comments as $comment)
            <div class="card mb-2 ">
                <div class="card-body">
                    {{$comment->content}}
                    <div class="d-flex justify-content-between align-item-center">
                        <small>posté le {{$comment->created_at->format('d/m/Y')}}</small>
                        <span class="badge badge-primary">{{ $comment->user->name}}</span>
                    </div>
                </div>
            </div>
        @empty
            <div class="alert alert-info">Aucun commentaire pour ce film</div>
        @endforelse
    
        <form action="{{ route('comments.store', $movie) }}" method="POST" class="mt-3">
        @csrf 
        <div class="form-group">
            <label for ="content"> Votre commentaire </label>
            <!-- recuperer l'id du film avec un input hidden -->
            <input type="hidden" name="commentable_id" value="{{ $movie->id }}"/> 
            <textarea class="form-control" @error('content') is-invalid @enderror" name="content" id ="content" rows="5"></textarea>
        @error('content')
            <div class="invalid-feedback"> {{ $errors->first('content') }}</div>
        @enderror
        </div>
        
        <button type="submit" class="btn btn-primary"> Soumettre mon commentaire </button>
        </form>
    </div>
    </div>
@endsection