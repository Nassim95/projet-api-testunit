<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class TodoList
 *
 * @property int $id
 * @property string $name
 * @property int $id_user
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class TodoList extends Model
{

	protected $casts = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime'
	];

	protected $fillable = [
		'name',
		'description'
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function items()
    {
        return $this->hasMany(Item::class)->orderBy('created_at', 'desc');
    }

    public function isValid()
    {
        $this->nameIsValid();
        $this->descriptionIsValid();

    }

    public function nameIsValid(){
        if (!empty($this->name)) {
            return $this->name;
        }
    }

    public function descriptionIsValid(){
        if (!empty($this->description)) {
            return $this->description;
        }
    }

    /**
     * @param Item $item
     * @return Item
     * @throws Exception
     */
    public function canAddItem(Item $item)
    {
        if (is_null($item)) {
            throw new Exception('litem est vide');
        }

        if ($item->isValid()) {
            throw new Exception('litem nest pas valide');
        }

        if ($this->getNumberItems() >= 10) {
            throw new Exception('Votre TodoList contient trop ditem');
        }

        $lastItem = $this->getLastItem();
        if (!is_null($this->getLastItem()) && Carbon::now()->subMinutes(30)->isBefore($lastItem->created_at)) {
            throw new Exception('Il faut patientez 30min entre lajout de chaque item');
        }

        return $item;
    }

    protected function getLastItem()
    {
        return $this->items->first();
    }

    protected function getNumberItems()
    {
        return $this->items->sizeof();
    }
}
