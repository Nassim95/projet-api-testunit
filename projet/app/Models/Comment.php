<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Comment
 * 
 * @property int $id
 * @property string $content
 * @property int $user_id
 * @property int $commentable_id
 * @property string $commentable_type
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Comment extends Model
{
	protected $table = 'comments';

	protected $casts = [
		'user_id' => 'int',
		'commentable_id' => 'int'
	];

	protected $fillable = [
		'content',
		'user_id',
		'commentable_id',
		'commentable_type'
	];
}
