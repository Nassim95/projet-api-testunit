<?php

/**
 * Created by Reliese Model.
 */

namespace App\Models;

use Exception;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class User
 *
 * @property int $id
 * @property string $lastname
 * @property string $name
 * @property string $email
 * @property int $age
 * @property Carbon $birthday
 * @property Carbon $email_verified_at
 * @property string $password
 * @property string $remember_token
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */

class User extends Model
{
	protected $table = 'users';

	protected $casts = [
		'age' => 'int'
	];

	protected $dates = [
		'email_verified_at'
	];

	protected $hidden = [
		'password',
		'remember_token'
	];

	protected $fillable = [
		'name',
		'email',
        'birthday',
		'email_verified_at',
		'password',
		'remember_token'
	];

	public function favoris()
    {
        return $this->hasMany('App\Favori','id_user');
    }

    public function currentsEpisodeShow()
    {
        return $this->hasMany('App\UsersCurrentEpisodeShow','id_user');
    }

    public function todoList()
    {
        return $this->hasOne(TodoList::class);
    }

    public function isValid()
    {
        $this->nameIsValid();
        $this->emailIsValid();
        $this->birthdayIsValid();
    }

    public function nameIsValid(){
        if (!empty($this->name)) {
            return $this->name;
        }
    }

    public function emailIsValid(){
        if (!empty($this->email) && filter_var($this->email, FILTER_VALIDATE_EMAIL)) {
            return $this->email;
        }
    }

    public function birthdayIsValid(){
        if (!empty($this->birthday) && (Carbon::now() - ($this->birthday)) >= 13) {
            return $this->birthday;
        }
    }
}
