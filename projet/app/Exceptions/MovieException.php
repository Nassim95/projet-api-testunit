<?php

namespace App\Exceptions;

use Illuminate\Foundation\Exceptions\Handler as ExceptionHandler;
use Throwable;

class MovieException extends ExceptionHandler{

    public function __construct($message, $code = 400)
    {
        parent::__construct($message, $code);
    }
    
}