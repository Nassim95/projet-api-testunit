<?php

/**
 * Created by Reliese Model.
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class UsersCurrentEpisodeShow
 * 
 * @property int $id
 * @property int $id_user
 * @property int $id_show
 * @property int $season
 * @property int $episode
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class UsersCurrentEpisodeShow extends Model
{
	protected $table = 'users_current_episode_shows';

	protected $casts = [
		'id_user' => 'int',
		'id_show' => 'int',
		'season' => 'int',
		'episode' => 'int'
	];

	protected $fillable = [
		'id_user',
		'id_show',
		'season',
		'episode'
	];

	public function user()
	{
		return $this->belongsTo('App\User','id_user');
	}
}
