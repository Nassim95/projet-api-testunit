<?php

/**
 * Created by Reliese Model.
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Support\Facades\Http;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Arr;


/**
 * Class Genre
 * 
 * @property int $id
 * @property string $name
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Genre extends Model
{
	protected $table = 'genres';

	protected $fillable = [
		'id',
		'name'
	];

    
	public static function getMoviesGenres()
    {
        $genreArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/genre/movie/list')
        ->json()['genres'];
		
		$genres = collect($genreArray)->map(function($data) {
			return (new Genre())->fill($data);
		});
        return $genres;
    }
    
    public static function getShowsGenres()
    {
        $genreArray = Http::withToken(
            config('services.tmdb.token')
        )
        ->get('https://api.themoviedb.org/3/genre/tv/list')
        ->json()['genres'];
        
        
		$genres = collect($genreArray)->map(function($data) {
			return (new Genre())->fill($data);
		});
        return $genres;
    }
}


