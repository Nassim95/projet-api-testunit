<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Show;
use App\Favori;
use Illuminate\Support\Facades\Auth;

use Illuminate\Database\Eloquent\Builder;

class ShowController extends Controller
{
    public function showTv($id){
        $show = (new Show())->show($id);
        $recommendedShows = self::recommended_shows();
        return view('show-tv', ['show' => $show, 'recommendedShows' => $recommendedShows]);
    }

    public static function recommended_shows()
    {
        $shows = Show::getPopular();

        $myShow = $shows->all();
        
        shuffle($myShow);

        $recommendedShows = array_slice($myShow, 0, 4);


        $favoris = Favori::whereNotNull('id_genre')->whereHas('user', function (Builder $query) {
            $query->where('id', '=', Auth::id());
        })->get();


        $favorisGenreId = collect($favoris)->map(function ($favori) {
            return $favori->id_genre;
        });


        $recommended = collect($recommendedShows)->filter(function($show) use ($favorisGenreId){
            $in = false;
            foreach ($show->genres as $genre) {
                if (in_array($genre->id, $favorisGenreId->all())) {
                    $in = true;
                    break;
                }
            }
            if ($in) {
                return $show;
            }
        });

        return $recommended;
    }
}
