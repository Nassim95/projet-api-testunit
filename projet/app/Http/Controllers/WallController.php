<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Post;
use App\Film;

class WallController extends Controller
{
    //

    public function index()
    {
        //$posts = App\Post::has('comments')->get();
        //$posts = Post::all();
        $films = Film::with('genre')->get();

        //$films = Film::All();
       // $books = App\Book::with('author')->get();


        $posts= Post::all();
        return view('wall',['posts' => $posts,'films' => $films]);
    }

     

    public function write(Request $request )
    {
        $post = new Post;
        $post->comment = $request->comment;
        $post->save();

        return redirect('wall');
    }

}
