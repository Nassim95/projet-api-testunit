<?php

namespace App\Http\Controllers;

use App\Movie;
use App\Show;
use App\Comment;
use Illuminate\Http\Request;

class CommentController extends Controller
{
    public function ___construct()
    {
        $this->middleware('auth');
    }
    public function store (Movie $movie)
    {
        request()->validate([
            'content' => 'required|min:5'
        ]);
        $comment = new Comment();
        $comment->commentable_id = request('commentable_id');
        $comment->content = request('content');
         /* ceci est degeulasse, c'etais pour differencier les commentaire de film et de series mais inutile */
        $comment->commentable_type= 'film/serie';
        $comment->user_id = auth()->user()->id;
        /* Inserer les commentaires */
        $comment->save();
        // $movie-> comments()->save($comment);
        return redirect()->route('showMovie', request('commentable_id'));
   }

   public function storeShow (Show $show)
    {
        request()->validate([
            'content' => 'required|min:5'
        ]);
        $comment = new Comment();
        $comment->commentable_id = request('commentable_id');
        $comment->content = request('content');
         /* ceci est degeulasse, c'etais pour differencier les commentaire de film et de series mais inutile */
        $comment->commentable_type= 'film/serie';
        $comment->user_id = auth()->user()->id;
        /* Inserer les commentaires */
        $comment->save();
        // $movie-> comments()->save($comment);
        return redirect()->route('showTv', request('commentable_id'));
   }
}
