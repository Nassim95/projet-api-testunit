<?php

namespace App\Http\Controllers;

use Exception;
use App\Http\Services\Tmdb;
use Illuminate\Http\Request;
use Faker\ORM\CakePHP\Populator;
use App\Exceptions\MovieException;
use Illuminate\Support\Facades\Http;

class MoviesController extends Controller
{
    //
    public function index()
    {   
        $tmdb = new Tmdb('services.tmdb.token');
        $popularMovies = $tmdb->getPopularMovies();
        $popularSeries = $tmdb->getPopularSeries();

        return view('movies', ['popularMovies'=>$popularMovies,'popularSeries'=>$popularSeries]);
    }

    public function getPopularMovies()
    {
        return (new Tmdb('services.tmdb.token'))->getPopularMovies();
    }

    public function getTopRated()
    {
        return (new Tmdb('services.tmdb.token'))->getTopRated();
    }

    public function getPopularSeries()
    {
        return (new Tmdb('services.tmdb.token'))->getPopularSeries();
    }

    public function getImage($poster_path, $width = 500)
    { 
        if(!is_numeric($width) || $width > 950){
            throw new Exception("la variable genre doit être numérique");
        }

        return (new Tmdb('services.tmdb.token'))->getImage($poster_path, $width);

    }

    public function getGenreName()
    {
        return (new Tmdb('services.tmdb.token'))->getGenreName();
    }

    public function getMovieByGenre($genre)
    {
        if(!is_numeric($genre)){
            throw new Exception("la variable genre doit être numérique");
        }elseif($genre < 0){
            throw new Exception("la variable genre doit être positive");
        }
        return (new Tmdb('services.tmdb.token'))->getMovieByGenre($genre);
    }

    private function throwException($message, $code = 400)
    {
        throw new MovieException($message, $code);
    }
}
