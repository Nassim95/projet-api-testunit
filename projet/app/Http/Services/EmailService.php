<?php

namespace App\Http\Services;

use App\Models\Item;
use App\Models\User;

class EmailService
{
    public function sendMail(User $user, Item $item)
    {
        $userTodos = new UserService;

        if($userTodos->getUserNumberTodo($user) == 8){
            return "Il ne reste plus que 2 item à votre TodoLit";
        }else{
            return "L'item : ".$item." a été ajouté à votre liste";
        }
    }
}
