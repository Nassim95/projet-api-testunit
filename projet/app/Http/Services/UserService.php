<?php

namespace App\Http\Services;

use App\Models\User;

class UserService
{
    public function getUserWithTodoList(User $user): User
    {
        return User::whereId($user->id)->with('todoList.items')->first();
    }

    public function getUserNumberTodo(User $user): int
    {
        return User::whereId($user->id)->with('todoList.items')->count();
    }
}
