<?php

namespace App\Http\Services;

use Exception;
use Illuminate\Support\Facades\Http;

class Tmdb{
    private $http;

    public function __construct($token = null)
    {
        if(($token == '' || $token === null)){
            throw new Exception('clé API vide');
        }
        $this->http = Http::withToken(
            config($token)
        );
    }

    public function getPopularMovies()
    {
        return $this->http
        ->get('https://api.themoviedb.org/3/movie/popular')
        ->json()['results'];
    }

    public function getTopRated()
    {
        return $this->http
        ->get('https://api.themoviedb.org/3/movie/top_rated')
        ->json()['results'];
    }

    public function getPopularSeries()
    {
        return $this->http
        ->get('https://api.themoviedb.org/3/tv/popular')
        ->json()['results'];
    }

    public function getImage($poster_path, $width = 500)
    {  
        return "https://image.tmdb.org/t/p/w$width/$poster_path";
    }

    public function getGenreName()
    {
        $genreArray = $this->http
        ->get('https://api.themoviedb.org/3/genre/movie/list')
        ->json()['genres'];

        $genres = collect($genreArray)->mapWithKeys(function($genre){
            return [$genre['id'] => $genre['name']];
        });  

        return $genres;
    }

    public function getGenreOfName($genreid)
    {
        if(!is_numeric($genreid)){
            throw new Exception("la variable genre doit être numérique");
        }elseif($genreid < 0){
            throw new Exception("la variable genre doit être positive");
        }

        return $this->http
        ->get('https://api.themoviedb.org/3/genre/movie/list')
        ->json()['genres'][$genreid];
    }

    public function getMovieByGenre($genre)
    {
        if(!is_numeric($genre)){
            throw new Exception("la variable genre doit être numérique");
        }elseif($genre < 0){
            throw new Exception("la variable genre doit être positive");
        }
        
        return $this->http
        ->get('https://api.themoviedb.org/3/movie/top_rated?with_genres='.$genre)
        ->json()['genres'];
    }

    public function getMovieByDate($date)
    {
        if(!is_numeric($date)){
            throw new Exception("la variable genre doit être numérique");
        } elseif ($date < 1895){
            throw new Exception("les films n'existaient pas encore hein !! On te vois");
        }
        
        return $this->http
        ->get('https://api.themoviedb.org/3/movie/top_rated?primary_release_year='.$date)
        ->json()['result'];
     
    }

    public function getMovieByPersone($persone)
    {
        if(!is_numeric($persone)){
            throw new Exception("la variable genre doit être numérique");
        } elseif ($persone < 0){
            throw new Exception("valeur negative nnon valide");
        }
        
        return $this->http
        ->get('https://api.themoviedb.org/3/movie/top_rated?with_people='.$persone)
        ->json()['result'];
     
    }

    public function getMovieByCountAverageVote($avg)
    {
        if(!is_numeric($avg)){
            throw new Exception("la variable genre doit être numérique");
        } elseif ($avg < 0){
            throw new Exception("les films n'existaient pas encore hein !! On te vois");
        }
        
        return $this->http
        ->get('https://api.themoviedb.org/3/movie/top_rated?vote_average.desc&vote_count.gte='.$avg)
        ->json()['result'];
     
    }


    /**
     * Set the value of http
     *
     * @return  self
     */ 
    public function setHttp($http)
    {
        $this->http = $http;

        return $this;
    }
}