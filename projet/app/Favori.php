<?php

/**
 * Created by Reliese Model.
 */

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

/**
 * Class Favori
 * 
 * @property int $id
 * @property int $id_user
 * @property int $id_show
 * @property int $id_genre
 * @property int $id_movie
 * @property Carbon $created_at
 * @property Carbon $updated_at
 *
 * @package App\Models
 */
class Favori extends Model
{
	protected $table = 'favoris';

	protected $casts = [
		'id_user' => 'int',
		'id_show' => 'int',
		'id_genre' => 'int',
		'id_movie' => 'int'
	];

	protected $fillable = [
		'id_user',
		'id_show',
		'id_genre',
		'id_movie'
	];

	public function user()
	{
		return $this->belongsTo('App\User','id_user');
	}

	public function genre()
	{
		return $this->belongsTo('App\Genre','id_user');
	}
}
